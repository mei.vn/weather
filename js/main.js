
function main() {


function load_weather_cr(){ 
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
        var url = url_lat(position.coords.latitude, position.coords.longitude);
       check_weather(url,function(data){
         var rs = decode_data(data);
        // //console.log(rs);
        var img_link = $('#img_link').val()+rs['icon'] + '.png';
        $('.favicon').attr('href', img_link)
         $('#cr_city').html(rs['name'] + ', ' + rs['country']);
      $('.we-icon').attr('src', img_link);
     $('.cr_temp_f').html('<b>' +  rs['t_f'] + '</b> °F');
      $('.cr_temp_c').html('<b>' + rs['t_c'] + '</b> °C') ;
      $('.cr_description').html(rs['description'] );
      $('.cr_sunrise').html('Sunrise at <b>'+ rs['sunrise']  +'</b>');
      $('.cr_sunset').html('Sunset at <b>'+ rs['sunset']  + '</b>');
       $('.cr_humidity').html('Humidity: <b>' + rs['humidity'] +'</b>');
       $('.cr_wind').html('Wind speed: <b>'+ rs['wind'] +'</b>');
         $('.cr_cloud').html('Cloudiness: <b>'+ rs['cloud'] + '</b>');
         if(rs['direction'] != '')
            $('.cr_determine').html(rs['determine'] + '. ' + rs['direction']);
          else $('.cr_determine').html(rs['determine']);
       // load header image
       // get current time:

         var date_stt = "clear";
          if(rs['description'].includes("Clear") ||  rs['description'].includes("clear")){
            date_stt = 'clear';
          }
          if( rs['description'].includes("Cloud") ||  rs['description'].includes("cloud")){
            date_stt = 'cloud';
          }
          if(rs['description'].includes("Rain") ||  rs['description'].includes("rain")){
            date_stt = 'rain';
          }
     
           if(date_stt == "clear" ){
             $('header').css('background-image', 'url(img/sun-morning.jpg)');
         }
           else if(date_stt == "cloud" ){
             $('header').css('background-image', 'url(img/cloud.jpg)');
         }
            else if(date_stt == "rain" ){
             $('header').css('background-image', 'url(img/rain.jpg)');
         }

        var hours =  new Date().getHours();
//console.log(hours)
       if(hours > 0 && hours < 10){
        $('.date-stt').html('Good Morning! Do you have a coffee?');
        if(date_stt == "clear")
          $('header').css('background-image', 'url(img/sun-morning.jpg)');

       }
         else if(hours >= 10 && hours < 12){
         $('.date-stt').html('Good Morning!');
          if(date_stt == "clear"){
             $('.cr_determine').html(rs['determine'] + ', Don’t forgot your hat.');
             $('header').css('background-image', 'url(img/noon.jpg)');
         }
       }
       else if(hours >= 12 && hours < 14){
         $('.date-stt').html('Good noon! Do you take a nap?');
         ////console.log(rs['description']);
          if(date_stt == "clear"){
            ////console.log(rs['description']);
           $('header').css('background-image', 'url(img/after-noon.jpg)');
          }
       }
        else if(hours >= 14 && hours < 17){
         $('.date-stt').html('Good afternoon!');
         if(date_stt == "clear")
         $('header').css('background-image', 'url(img/after-noon.jpg)');
       }
          else if(hours >= 17 && hours < 21){
         $('.date-stt').html('Good evening! Do you have an dinner?');
         if(date_stt != "rain")
         $('header').css('background-image', 'url(img/evening.jpg)');
       }
          else if(hours >= 21){
         if(date_stt != "rain" )
          $('header').css('background-image', 'url(img/evening.jpg)');
         $('.date-stt').html("It's time to bed! Good night.");

       }
  
       });
          });
    }
}
function set_data(data){
       var res = decode_data(data);
     // //console.log(data.name);
        // //console.log(response);
        $('.results').html('<b>' + city + '</b> weather ');
     $('.temp_f').html('<b>' +  res['t_f'] + '</b>' +'°F');
      $('.temp_c').html('<b>' + res['t_c'] + '</b>' +'°C');
      $('.description').html(res['description'] );
      $('.sunrise').html('<b>'+ res['sunrise']  +'</b>');
      $('.sunset').html('<b>'+ res['sunset']  + '</b>');
       $('.determine').html(res['determine']);
       if(res['direction']){
         $('.direction').html(res['direction'] );
          $('.r_direction').show();
       }
       $('.humidity').html('<b>' + res['humidity'] + '<b>');
         $('.cloud').html('<b>' + res['cloud'] + '<b>');
       $('.wind ').html('<b>' + res['wind'] + '<b>');
       $('.tb-res').show();
}

(function () {
   'use strict';

$(document).ready(function(event){
    var datetime = null,
        date = null;
var update = function () {
    date = moment(new Date())
    $('.cr-time').html(date.format('dddd, MMMM Do YYYY, h:mm:ss a'));
};

    datetime = $('#datetime')
    update();
    setInterval(update, 1000);
     load_weather_cr();
});
Date.prototype.getFullMinutes = function () {
   if (this.getMinutes() < 10) {
       return '0' + this.getMinutes();
   }
   return this.getMinutes();
};
    var loading = $("#LoadingImage");
$('.btn-Checkweather').click(function(event){
  event.preventDefault();
   $("#LoadingImage").show();
  var lat = $('#lat').val();
  var lon = $('#lon').val();
  var city = $('#city').val();
if(location != '' && location != ','){
    var url = url_lat(lat, lon); 
    var data ="";
    // check first
   check_weather(url,function(data){
    if(!data){
      url = url_city(city);
           check_weather(url,function(data1){
           if(!data1){
            $('.results').html('Not found');
         }
         else{
          set_data(data1);
         }
       })
    }
    else{
      set_data(data);

    }
   });
       $("#LoadingImage").hide();


}
  else   {$('.results').html('Empty data');
  $('.tb-res').hide();
}


});

   /* ==============================================
  	Testimonial Slider
  	=============================================== */ 

  	$('a.page-scroll').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            $('html,body').animate({
              scrollTop: target.offset().top - 40
            }, 900);
            return false;
          }
        }
      });

    /*====================================
    Show Menu on Book
    ======================================*/
    $(window).bind('scroll', function() {
        var navHeight = $(window).height() - 500;
        if ($(window).scrollTop() > navHeight) {
            $('.navbar-default').addClass('on');
        } else {
            $('.navbar-default').removeClass('on');
        }
    });

    $('body').scrollspy({ 
        target: '.navbar-default',
        offset: 80
    })


  	/*====================================
    Portfolio Isotope Filter
    ======================================*/
    $(window).load(function() {
        $('.cat a').click(function() {
            $('.cat .active').removeClass('active');
            $(this).addClass('active');
            var selector = $(this).attr('data-filter');
            $container.isotope({
                filter: selector,
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false
                }
            });
            return false;
        });

    });

}());


}
main();