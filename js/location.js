      var placeSearch, autocomplete;
        var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };
      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
      (document.getElementById('autocomplete')),
            {types: ['(regions)']});
        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
      }
      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

  // ////console.log(place);
  $('#lat').val(place.geometry.location.lat());
  $('#lon').val(place.geometry.location.lng());
   city = place.address_components[0].long_name;
   var country =""
   // if(place.address_components.length > 2)
   //   city = place.address_components[1].long_name;
   // else  city = place.address_components[0].long_name;
   $.each(place.address_components, function(index, val) {
      if(val.types[0] == "country"){
        country = val.short_name;
      }
      else if(val.types[0] == "locality"){
        city = val.long_name;
      }
      else if(val.types[0] == "administrative_area_level_1"){
        city = val.long_name;
      }
   });
  
 $('#city').val(city + ',' + country);
  ////console.log(city + ',' + country);

      }
      function decodeTime(){
   var hours =  new Date().getHours();
    // get current time:
       if(hours > 0 && hours < 10){
        return 'morning';

       }
         else if(hours >= 10 && hours < 12){
          return 'morning-2';

       }
       else if(hours >= 12 && hours < 14){
        return 'noon';
       }
        else if(hours >= 14 && hours < 17){
          return 'noon-2';
       }
          else if(hours >= 17 && hours < 21){
            return 'evening';
       }
          else if(hours >= 21 && hours < 0){
            return 'evening-2'
       }
}
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
    function degToDirection($num)
 {
  $arr = ["NNE" ,"NE","ENE","E","ESE", "SE", "SSE","S","SSW","SW","WSW","W","WNW","NW","NNW"];
  $res = {"NNE" : "North-North-East" ,"NE" : "North-East","ENE" : "East-North-East" ,"E" : "East","ESE" : "East-South-East", "SE" : "South-East", "SSE" : "South-South-East","S" : "South","SSW" : "South-South-West","SW" : "South-West","WSW" : "West-South-West","W":"West" ,"WNW" :"West-North-West","NW" : "North-West","NNW" : "North-North-West"};
  if ($num > 348.75 || $num <= 11.25) {
   return $dir = "North";
  }
   else if (Number.isInteger($num/22.5) == true) {
   $val= Math.floor(($num/22.5)-.5);
   $dir = $arr[$val - 1];
   return $res[$dir];
  } else {
   $val= Math.floor(($num/22.5)-.5);
  $dir = $arr[($val)];
  
  }
    return $res[$dir];
 }
function decode_data(data){
    var res = data;
    if(data != '-1'){
      res = [];
      var weather = data.weather;
     res['name'] = data.name;
     res['country'] = data.sys.country;
       res['t_f'] = Math.round(data.main.temp * 9/5 - 459.67);
       res['t_c'] = Math.round(data.main.temp - 273.15);
       res['t_c_min'] = Math.round(data.main.temp_min - 273.15);
        res['t_c_max'] = Math.round(data.main.temp_max - 273.15);
       var t_sunset = new Date(data.sys.sunset*1000);
       var t_sunrise = new Date(data.sys.sunrise*1000);
         res['sunset'] = (t_sunset.getHours() + ':' + t_sunset.getFullMinutes());
         res['sunrise']  = (t_sunrise.getHours() + ':' + t_sunrise.getFullMinutes());
       var main_de = '';
        res['description'] = '';
          res['humidity'] = data.main.humidity + ' %';
          res['cloud'] = data.clouds.all + ' %';
          res['main'] = data.main;
          res['icon'] = data.weather[0].icon;
          res['rain'] = data.rain ? data.rain : false;
          res['snow'] = data.snow ? data.snow : false;
         res['wind_deg'] =  degToDirection(data.wind.deg) ? ', ' +  degToDirection(data.wind.deg)  : '';
         res['wind'] = data.wind.speed + ' m/s' + res['wind_deg'] ;
         // ////console.log(res['wind_deg']);
       $.each(data.weather, function(index, val){
        if(index == data.weather.length - 1)
          res['description']  = res['description'] + val.description;
        else res['description'] = res['description'] +  val.description + ', ';
       });
       // time
 var time_stt =  decodeTime();
// end time
      res['direction'] = '';
      if(res['t_c'] <= 16 ){
        res['temp'] = 'cold';
          res['direction'] ='Don’t forgot your coat and don’t get sick ';
      }
      else if( 16 < res['t_c'] && res['t_c'] <= 21){
        res['temp'] = 'cool';
        if(res['description'].includes("clear sky") || res['main'] == 'clear' )
            res['direction'] = 'It’s a nice day out!';
      }
      else if( 21 < res['t_c'] && res['t_c'] <= 26){
        res['temp'] = 'warm';
      }
      else if( 26 < res['t_c'] && res['t_c'] <= 33){
        res['temp'] = 'quite hot';
        if(time_stt == "morning" ||time_stt == "morning-2" ||time_stt == "noon" ||time_stt == "noon-2")
         res['direction'] = 'Don’t forgot your hat.';
      }
      else if(33< res['t_c'] && res['t_c'] < 60 ){
         res['temp'] = 'hot';
          if(time_stt == "morning")
         res['direction'] = 'Don’t for get your sunscreen!';
      }
      else if(res['t_c'] > 60){
        res['temp'] = 'very hot';
        if(time_stt == "morning" ||time_stt == "morning-2" ||time_stt == "noon" ||time_stt == "noon-2")
         res['direction'] = 'Don’t for get your sunscreen!';
      }
      else res['temp'] = 'normal';
      res['determine'] = 'It’s a '+res['temp']+' day';
      if(res['description'].includes("rain")){
        res['direction'] = 'It will be rain, you should bring your umbrella';
        }
        // time
      res['description'] = capitalizeFirstLetter( res['description']);
   }
   return res;
}
function getLocation(callback){
      $.ajax({
            url: "https://geoip-db.com/jsonp",
            jsonpCallback: "callback",
            dataType: "jsonp",
            success: function( location ) {
              callback(location);  
            }
        });  
}


function url_city(city){
    return 'http://api.openweathermap.org/data/2.5/weather?&q='+city+'&appid=';
}
function url_lat(lat, lng){
    return 'http://api.openweathermap.org/data/2.5/weather?lat='+lat+'&lon='+lng+'&appid=';
}

    function check_weather(url,callback){
    var appid = 'beafdc37d7d1c891f4322d9280fb7a12';
    url =  url + appid;
    $.ajax({
  url: url,
  type: 'GET',
  success:function(res){
    $("#LoadingImage").hide();
    $('#weather').val(res);
    callback(res);
  },
  error :function(res){
     $("#LoadingImage").hide();
     // console.log(res);
      callback(false);
  }
});
    }


